package com.example.postsservice.db;

import com.arangodb.ArangoDB;
import com.arangodb.springframework.annotation.EnableArangoRepositories;
import com.arangodb.springframework.config.ArangoConfiguration;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableArangoRepositories(basePackages = {"com.example.postsservice"})
public class CustomArangoConfiguration implements ArangoConfiguration {

    @Value("${arangodb.host}")
    private String arangodbHost;

    @Value("${arangodb.port}")
    private int arangodbPort;

    @Value("${arangodb.pass}")
    private String arangodbPass;

    @Override
    public ArangoDB.Builder arango() {
        return new ArangoDB.Builder().host(arangodbHost, arangodbPort).user("root").password(arangodbPass);
    }

    @Override
    public String database() {
        return "posts-db";
    }
}
