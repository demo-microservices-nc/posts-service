package com.example.postsservice.db;

import org.springframework.data.repository.CrudRepository;

public interface PostsRepository extends CrudRepository<Post, String> {

}
