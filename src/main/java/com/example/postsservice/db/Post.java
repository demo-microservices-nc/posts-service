package com.example.postsservice.db;

import com.arangodb.springframework.annotation.Document;
import org.springframework.data.annotation.Id;

import java.util.List;

@Document("post")
public class Post {

    @Id
    private String id;

    public Post(String content) {
        this.content = content;
    }

    private String content;

    public String getContent() {
        return content;
    }

    public String getId() {
        return id;
    }
}
