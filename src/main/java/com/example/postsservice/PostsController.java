package com.example.postsservice;

import com.example.postsservice.db.Post;
import com.example.postsservice.db.PostsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@RestController
public class PostsController {

    private final RestTemplate restTemplate;

    private final PostsRepository repository;

    @Autowired
    public PostsController(RestTemplate restTemplate, PostsRepository repository) {
        this.restTemplate = restTemplate;
        this.repository = repository;
    }

    @RequestMapping(method = GET, value = "/getAllPosts")
    public Iterable<Post> getAllPosts() {
        return repository.findAll();
    }

    @RequestMapping(method = GET, value = "/getPostById/{postId}")
    public Map<String, Object> getPostById(@PathVariable String postId) {
        Map<String, Object> responseMap = new HashMap<>();

        Post post = repository.findById(postId).orElseGet(() -> new Post("Empty"));

        responseMap.put("post", post);


        List comments = restTemplate.getForObject(
                "http://comments-service/getById/{postId}", List.class, post.getId());
        if (comments != null) {
            responseMap.put("comments", comments);
        }

        return responseMap;
    }

    @RequestMapping(method = GET, value = "/addPost/{content}")
    public Post addPost(@PathVariable String content) {
        Post post = new Post(content);
        repository.save(post);

        return post;
    }
}
